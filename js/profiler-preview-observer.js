/**
 * @file
 * Behaviours for profiler observer.
 */

// Namespace.
var qToolsProfilerPreviewObserver = {
  'loaded': false,
  'requests': [],
  'watchers': []
};

(function () {
  var observer = qToolsProfilerPreviewObserver;

  /**
   * Triggers set report content.
   */
  observer.setReportContent = function (report, content) {
    // Dispatch initial sync event.
    observer.dispatch('onSetReportContent', {
      'report': report,
      'content': content
    });
  };

  /**
   * Triggers select report event.
   */
  observer.reportSelected = function (report) {
    // Dispatch initial sync event.
    observer.dispatch('onSelectReport', {
      'report': report
    });
  };

  /**
   * Subscribe to observer events.
   */
  observer.subscribe = function (callbacks) {
    observer.watchers.push(callbacks);

    // Dispatch initial sync event.
    observer.dispatchOne(callbacks, 'onUpdateRequests', {
      'requests': observer.requests
    });
  };

  /**
   * Subscribe to observer events.
   */
  observer.dispatch = function (event, data) {
    for (var i = 0; i < observer.watchers.length; i++) {
      observer.dispatchOne(observer.watchers[i], event, data);
    }
  };

  /**
   * Subscribe to observer events.
   */
  observer.dispatchOne = function (callbacks, event, data) {
    if (callbacks[event]) {
      callbacks[event](data);
    }
  };

  /**
   * Add new command to populate summary for request.
   */
  Drupal.AjaxCommands.prototype.qtoolsProfilerPreviewSummary = function (ajax, response, status) {
    if (status === 'success') {
      var trackingId = response.trackingId;
      var prepend = false;
      if (!observer.requests[trackingId]) {
        trackingId = observer.pushRequest(response.name);
        prepend = true;
      }
      observer.updateRequest(trackingId, response.requestId, response.summary, prepend);
    }
  };

  // Hook every ajax request.
  if (XMLHttpRequest) {
    var origOpen = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function() {

      // Skip technical requests.
      if (observer.isRequestTracked(arguments)) {
        var requestName = arguments[0] + ':' + arguments[1].split('?')[0];
        var trackingId = observer.pushRequest(requestName);
        this.addEventListener('load', function() {
          var requestId = this.getResponseHeader('X-QTools-Profiler-RequestId');
          var requestSummary = this.getResponseHeader('X-QTools-Profiler-RequestSummary');
          if (requestId) {
            observer.updateRequest(trackingId, requestId, requestSummary);
          }
        });
      }

      // Run original request.
      origOpen.apply(this, arguments);
    };
  }

  /**
   * Check if this request should be tracked.
   */
  observer.isRequestTracked = function(args) {
    var request_is_tracked = true;

    // Do not track profiler own requests.
    if (args[1].indexOf('/qtools_profiler/') !== -1) {
      request_is_tracked = false;
    }

    // Do not track external requests unless they match domain of current site.
    if (args[1].indexOf('//') !== -1 && args[1].indexOf('//' + window.location.hostname) === -1) {
      request_is_tracked = false;
    }

    return request_is_tracked;
  };

  /**
   * Push new request.
   */
  observer.pushRequest = function(path, requestId) {
    var trackingId = observer.requests.length;
    var name = '../' + path.split('?')[0].replace(/\/$/,'').split('/').slice(-1)[0];
    observer.requests.push({
      'name': name,
      'path': path,
      'requestId': requestId,
      'trackingId': trackingId
    });

    observer.dispatch('onPushRequest', {
      'name': name,
      'path': path,
      'requestId': requestId,
      'trackingId': trackingId
    });
    observer.dispatch('onUpdateRequests', {
      'requests': observer.requests
    });
    return trackingId;
  };

  /**
   * Update request info.
   */
  observer.updateRequest = function(trackingId, requestId, requestSummary, prepend) {
    var summary = requestSummary ? JSON.parse(requestSummary) : [];
    observer.requests[trackingId].requestId = requestId;
    observer.requests[trackingId].summary = summary;
    observer.requests[trackingId].prepend = prepend || false;

    observer.dispatch('onUpdateRequest', {
      'trackingId': trackingId,
      'requestId': requestId,
      'summary': summary,
      'prepend': prepend
    });
    observer.dispatch('onUpdateRequests', {
      'requests': observer.requests
    });
  };


  // Push main request immediately.
  observer.pushRequest(window.location.href, drupalSettings.qtools_profiler.request_id);

  /**
   * Handle pageLoad.
   */
  observer.handlePageLoad = function (context, settings) {
    if (!observer.loaded && settings.qtools_profiler.request_id) {
      observer.loaded = true;

      // Request profiler info for main page.
      var pricesAjax = Drupal.ajax({
        url: '/admin/config/development/qtools_profiler/preview_summary/' + settings.qtools_profiler.request_id + '/0'
      });
      pricesAjax.execute();
    }
  };

  /**
   * Register behavior.
   */
  Drupal.behaviors.qToolsProfilerPreviewObserver = {
    attach: function (context, settings) {
      observer.handlePageLoad(context, settings);
    }
  };

})();