/**
 * @file
 * Behaviours for profiler wrapper.
 */

// Namespace.
var qToolsProfilerPreviewIframe = {
  'currentFold': '-',
  'requestId': null,
  'reportId': null,
  'loaded': null,
  'inspector': null,
  'requests': [],
  'reports': [],
  'wrapper': null,
  'header': null,
  'counter': null,
  'content': null
};

(function ($) {
  var preview = qToolsProfilerPreview;
  var previewIframe = qToolsProfilerPreviewIframe;
  var observer = qToolsProfilerPreviewObserver;

  /**
   * On requests list update.
   */
  previewIframe.onUpdateRequests = function (requests) {
    previewIframe.counter.text(requests.length);
  };

  /**
   * On report select.
   */
  previewIframe.onSelectReport = function (reportId) {
    previewIframe.reportId = reportId;
    previewIframe.saveState();
  };

  /**
   * Message handlers.
   */
  previewIframe.handleMessage = {};

  /**
   * Handle minimize message.
   */
  preview.handleMessage['MINIMIZE'] = function(data, e) {
    previewIframe.minimize();
  };

  /**
   * Handle maximize message.
   */
  preview.handleMessage['MAXIMIZE'] = function(data, e) {
    previewIframe.maximize();
  };

  /**
   * Handle normalize message.
   */
  preview.handleMessage['NORMALIZE'] = function(data, e) {
    previewIframe.normalize();
  };

  /**
   * Creates visual elements.
   */
  previewIframe.createVisuals = function (context, settings) {
    // Build visuals.
    var counter = '<span id="qtools-profiler-preview-counter">0</span>';
    var header = '<div id="qtools-profiler-preview-header">' + counter + '</div>';
    var content = '<iframe id="qtools-profiler-preview-content" />';
    var wrapper = '<div id="qtools-profiler-preview-wrapper" class="qtools-profiler-minimized">' + header + content + '</div>';
    $('body').append(wrapper);

    // Shortcuts.
    previewIframe.wrapper = $('#qtools-profiler-preview-wrapper');
    previewIframe.content = $('#qtools-profiler-preview-content');
    previewIframe.header = $('#qtools-profiler-preview-header');
    previewIframe.counter = $('#qtools-profiler-preview-counter');

    // Page cache status.
    var pageCacheStatus = $('#qtools-profiler-cache-status').attr('content');
    if (pageCacheStatus.indexOf('HIT:') !== -1) {
      previewIframe.counter.css('color', 'LimeGreen');
    }
    else if (pageCacheStatus.indexOf('MISS:') !== -1) {
      previewIframe.counter.css('color', 'LightCoral');
    }

    if (pageCacheStatus.indexOf(':HIT') !== -1) {
      previewIframe.counter.css('border', '2px solid LimeGreen');
    }
    else if (pageCacheStatus.indexOf(':MISS') !== -1) {
      previewIframe.counter.css('border', '2px solid LightCoral');
    }

    var pageStatusColor = $('#qtools-profiler-page-status-color').attr('content') || "";
    if (pageStatusColor !== "") {
      previewIframe.header.css('background-color', pageStatusColor);
    }

    // Set up receiver.
    preview.iframeReceiver = previewIframe.content[0].contentWindow;

    // Attach actions.
    previewIframe.header.click(function() {
      previewIframe.normalize();
    });

    // Subscribe to observer.
    observer.subscribe({
      'onUpdateRequests': function (data) {
        previewIframe.onUpdateRequests(data.requests);
      },
      'onSelectReport': function (data) {
        previewIframe.onSelectReport(data.report);
      }
    });
  };

  /**
   * Normalize preview area size.
   */
  previewIframe.normalize = function() {
    previewIframe.setFold('');
  };

  /**
   * Minimize preview area size.
   */
  previewIframe.minimize = function() {
    previewIframe.setFold('-');
  };

  /**
   * Maximize preview area size.
   */
  previewIframe.maximize = function() {
    previewIframe.setFold('+');
  };

  /**
   * Sets new fold status.
   */
  previewIframe.setFold = function (newFold) {
    // React app we have currently does not support IE, and i don't wan to make it happen.
    if (qtools.isIe) {
      return;
    }

    // Set iframe content if not yet.
    if (newFold !== '-' && previewIframe.content.attr('src') === undefined) {
      previewIframe.content.attr('src', "/admin/config/development/qtools_profiler/preview_app");
    }

    if (newFold === '+') {
      previewIframe.wrapper.addClass('qtools-profiler-maximized').removeClass('qtools-profiler-minimized');
    }
    else if (newFold === '-') {
      previewIframe.wrapper.addClass('qtools-profiler-minimized').removeClass('qtools-profiler-maximized');
    }
    else {
      newFold = '';
      previewIframe.wrapper.removeClass('qtools-profiler-minimized').removeClass('qtools-profiler-maximized');
    }
    previewIframe.currentFold = newFold;

    // Saves state.
    previewIframe.saveState();
  };

  /**
   * Saves status of inspector.
   */
  previewIframe.saveState = function () {
    var options = { expires: 385, path: '/'};
    var state = {
      'fold': previewIframe.currentFold,
      'reportId': previewIframe.reportId
    };
    $.cookie('qtools-profiler-preview-iframe-state', JSON.stringify(state), options);
  };

  /**
   * Restore inspector state.
   */
  previewIframe.restoreState = function () {
    var stateCookie = $.cookie('qtools-profiler-preview-iframe-state') || '';
    if (stateCookie !== '') {
      var state = JSON.parse(stateCookie);
      previewIframe.setFold(state.fold);
      preview.sendForceReport(state.reportId);
    }
  };

  /**
   * Handle pageLoad.
   */
  previewIframe.handlePageLoad = function (context, settings) {
    if (!previewIframe.loaded && settings.qtools_profiler.request_id) {
      previewIframe.loaded = true;

      // Create visuals.
      previewIframe.createVisuals();

      // Restore state.
      previewIframe.restoreState();
    }
  };

  /**
   * Register behavior.
   */
  Drupal.behaviors.qToolsProfilerPreviewIframe = {
    attach: function (context, settings) {
      previewIframe.handlePageLoad(context, settings);
    }
  };

})(jQuery);