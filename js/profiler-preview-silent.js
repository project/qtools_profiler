/**
 * @file
 * Behaviours for profiler wrapper.
 */

// Namespace.
var qToolsProfilerPreview = {
  'loaded': null,
  'defaultReport': null,
  'inspector': null,
  'requests': [],
  'reports': [],
  'iframeReceiver': null
};

(function () {
  var preview = qToolsProfilerPreview;
  var observer = qToolsProfilerPreviewObserver;
  var log = !!qtools.cookieGet('qToolsProfilerPreview.log');

  /**
   * Sends updated requests list to inspector.
   */
  preview.onUpdateRequests = function (requests) {
    // Save request info locally for late inspector connections.
    preview.requests = requests;
    preview.sendRequestsList(requests);
  };

  /**
   * Sends report content to inspector.
   */
  preview.onSetReportContent = function (data) {
    preview.sendReportContent(data.report, data.content || {});
  };

  /**
   * Sends report content to inspector.
   */
  preview.sendForceReport = function (report) {
    preview.defaultReport = report;
    preview.sendMessage('FORCE_REPORT', {
      'report': report
    });
  };


  /**
   * Sends report content to inspector.
   */
  preview.sendReportContent = function (report, content) {
    preview.sendMessage('REPORT_CONTENT_SET', {
      'report': report,
      'content': content
    });
  };

  /**
   * Sends request list to inspector.
   */
  preview.sendRequestsList = function (requests) {
    preview.sendMessage('REQUESTS_SET', requests);
  };

  /**
   * Sends request list to inspector.
   */
  preview.sendReportsList = function (reports) {
    preview.sendMessage('REPORTS_SET', reports);
  };

  /**
   * Message handlers.
   */
  preview.handleMessage = {};

  /**
   * Handle connect message.
   */
  preview.handleMessage['CONNECT'] = function(data, e) {
    preview.inspector = e.data.data;
    log && console.log('Inspector connected :', preview.inspector);
    preview.sendRequestsList(preview.requests);
    preview.sendReportsList(preview.reports);
    preview.sendForceReport(preview.defaultReport);
  };

  /**
   * Handle report selection message.
   */
  preview.handleMessage['SELECT_REPORT'] = function(data, e) {
    preview.reportSelected(data);
  };

  /**
   * Sends message to preview content frame.
   */
  preview.sendMessage = function (type, data) {
    var message = {
      'type': type,
      'data': data,
      'version': 1,
      'source': 'preview',
      'inspector': preview.inspector
    };
    if (preview.inspector) {
      log && console.log('sendMessage', type, message);
      if (preview.inspector.mode === 'devtools') {
        // This message will be relayed via content-js from extension.
        window.postMessage(message);
      }
      else if (preview.inspector.mode === 'iframe' && preview.iframeReceiver) {
        // We post this message directly to iframe container.
        preview.iframeReceiver.postMessage(message, '*');
      }
      else {
        log && console.log('sendMessage failed', type, data);
      }
    }
    else {
      log && console.log('sendMessage declined', type, data);
    }
  };

  /**
   * Select report.
   */
  preview.reportSelected = function(data) {
    observer.reportSelected(data.report);
  };

  /**
   * Handle pageLoad.
   */
  preview.handlePageLoad = function (context, settings) {
    if (!preview.loaded && settings.qtools_profiler.request_id) {
      preview.loaded = true;
      // preview.requestId = settings.qtools_profiler.request_id;
      preview.reports = settings.qtools_profiler.reports;

      // Starts listening window messages.
      window.addEventListener('message', function(e) {
        if (e.data && e.data.type && e.data.source !== 'preview') {
          if (preview.handleMessage[e.data.type]) {
            preview.handleMessage[e.data.type](e.data.data, e);
          }
          else {
            log && console.log('Unhandled message', e);
          }
        }
      });

      // Subscribe to observer.
      observer.subscribe({
        'onUpdateRequests': function (data) {
          preview.onUpdateRequests(data.requests);
        },
        'onSetReportContent': function (data) {
          preview.onSetReportContent(data);
        }
      });
    }
  };

  /**
   * Register behavior.
   */
  Drupal.behaviors.qToolsProfilerPreview = {
    attach: function (context, settings) {
      preview.handlePageLoad(context, settings);
    }
  };

})();