/**
 * @file
 * Behaviours for profiler observer.
 */

(function () {

  // Register ajax command.
  if (qToolsProfilerPreviewObserver && Drupal.AjaxCommands) {
    var observer = qToolsProfilerPreviewObserver;

    /**
     * Add new command to populate summary for request.
     */
    Drupal.AjaxCommands.prototype.qtoolsProfilerPreviewSummary = function (ajax, response, status) {
      if (status === 'success') {
        var trackingId = response.trackingId;
        var prepend = false;
        if (!observer.requests[trackingId]) {
          trackingId = observer.pushRequest(response.name);
          prepend = true;
        }
        observer.updateRequest(trackingId, response.requestId, response.summary, prepend);
      }
    };

    /**
     * Handle pageLoad.
     */
    observer.handlePageLoad = function (context, settings) {
      if (!observer.loaded && settings.qtools_profiler.request_id) {
        observer.loaded = true;

        // Request profiler info for main page.
        var ajax = Drupal.ajax({
          url: '/admin/config/development/qtools_profiler/preview_summary/' + settings.qtools_profiler.request_id + '/0'
        });
        ajax.execute();
      }
    };

    /**
     * Register behavior.
     */
    Drupal.behaviors.qToolsProfilerPreviewObserver = {
      attach: function (context, settings) {
        observer.handlePageLoad(context, settings);
      }
    };

  }
})();