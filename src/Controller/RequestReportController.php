<?php

namespace Drupal\qtools_profiler\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\RendererInterface;
use Drupal\qtools_profiler\Ajax\PreviewSummaryCommand;
use Drupal\qtools_profiler\PerformanceService;
use Drupal\qtools_profiler\ReportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RequestReportController.
 *
 * @package Drupal\qtools_profiler\Controller
 */
class RequestReportController extends ControllerBase {

  /**
   * Drupal\qtools_profiler\PerformanceService.
   *
   * @var \Drupal\qtools_profiler\PerformanceService
   */
  protected $performanceService;

  /**
   * Drupal\qtools_profiler\ReportService.
   *
   * @var \Drupal\qtools_profiler\ReportService
   */
  protected $reportService;

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    PerformanceService $performanceService,
    ReportService $reportService,
    FormBuilderInterface $formBuilder,
    ModuleHandlerInterface $moduleHandler,
    RendererInterface $renderer
  ) {
    $this->performanceService = $performanceService;
    $this->reportService = $reportService;
    $this->formBuilder = $formBuilder;
    $this->moduleHandler = $moduleHandler;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('qtools_profiler.performance'),
      $container->get('qtools_profiler.report'),
      $container->get('form_builder'),
      $container->get('module_handler'),
      $container->get('renderer')
    );
  }

  /**
   * Returns page content.
   *
   * @return array
   *   Page array.
   */
  public function pageBuild(Request $request) {

    // Get filter values.
    $route = $request->query->get('route');

    // Get filter form.
    $build['form'] = $this->formBuilder->getForm('\Drupal\qtools_profiler\Form\ReportFilterForm');

    // Get report table.
    $filters = [
      'route' => $route,
    ] + $this->reportService->getReportFilters();
    $header = $this->reportService->reportRequestTableHeader($filters);
    $query = $this->reportService->reportRequestBaseQuery($filters);
    $results = $query->execute()->fetchAll();
    $rows = $this->reportService->reportRequestProcessResults($results, $filters);

    $build['table'] = [
      '#type' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#cache' => [
        'max-age' => -1,
      ],
    ];

    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;
  }

  /**
   * Returns detailed profile page.
   *
   * @return array
   *   Page array.
   */
  public function pageBuildProfile(Request $request, $id) {
    return $this->performanceService->buildProfilePage($request, $id);
  }

  /**
   * Returns request summary.
   */
  public function previewSummary(Request $request, $id, $tracking_id) {
    // Prepare list of requests we need to get info for.
    $params[$tracking_id] = $id;
    $response = new AjaxResponse();

    // Get all pending redirect ids.
    $ids = $this->performanceService->getRedirectIds();
    foreach ($ids as $pos => $redirectId) {
      $params['r' . $pos] = $redirectId;
    }

    // Push info about all requests.
    foreach ($params as $tid => $rid) {
      $name = $this->performanceService->getRequestName($rid);
      $summary = $this->performanceService->getRequestSummary($rid);
      $response->addCommand(new PreviewSummaryCommand($tid, $rid, $name, $summary));
    }

    // Gather additional commands to send with preview info response.
    $this->moduleHandler->alter(PerformanceService::HOOK_REQUEST_SUMMARY_RESPONSE_ALTER, $response, $id);

    return $response;
  }

  /**
   * Returns preview app summary.
   */
  public function previewApp(Request $request) {
    $build_path = '/' . drupal_get_path('module', 'qtools_profiler') . '/react-app/chrome-extension/build';
    $build = [
      '#theme' => 'qtools_profiler_preview_iframe',
      '#build_path' => $build_path,
    ];

    $response = new HtmlResponse();
    $response->setContent($this->renderer->renderRoot($build));

    return $response;
  }

}
