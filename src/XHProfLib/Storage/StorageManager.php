<?php

namespace Drupal\qtools_profiler\XHProfLib\Storage;

/**
 * Class StorageManager
 */
class StorageManager {

  /**
   * @var array
   */
  private $storages;

  /**
   * @return array
   */
  public function getStorages() {
    $output = array();

    /** @var \Drupal\qtools_profiler\XHProfLib\Storage\StorageInterface $storage */
    foreach ($this->storages as $id => $storage) {
      $output[$id] = $storage->getName();
    }

    return $output;
  }

  /**
   * @param \Drupal\qtools_profiler\XHProfLib\Storage\StorageInterface $storage
   */
  public function addStorage($id, StorageInterface $storage) {
    $this->storages[$id] = $storage;
  }

}
