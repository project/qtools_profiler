<?php

namespace Drupal\qtools_profiler\XHProfLib\Parser;

interface ParserInterface {

  public function parse();
}
