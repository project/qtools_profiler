<?php

namespace Drupal\qtools_profiler\XHProfLib\Parser;

use Drupal\qtools_profiler\XHProfLib\Run;
use Drupal\qtools_profiler\XHProfLib\Parser\SymbolsSorter;

class Parser extends BaseParser {

  /**
   * @param $run
   * @param $sort
   * @param $symbol
   */
  public function __construct(Run $run, $sort, $symbol) {
    parent::__construct($run, $sort, $symbol);

    $this->diff_mode = FALSE;
  }

  /**
   * @return array
   */
  public function parse() {
    if (!empty($this->symbol)) {
      $symbols = $this->trimRun($this->run->getSymbols(), $this->symbol);
    }
    else {
      $symbols = $this->run->getSymbols();
    }

    $data = $this->computeFlatInfo($symbols);

    SymbolsSorter::sort($data, $this->sort);

    return $data;
  }

}
