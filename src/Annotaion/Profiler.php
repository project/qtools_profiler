<?php

namespace Drupal\qtools_profiler\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Profiler annotation object.
 *
 * @Annotation
 */
class Profiler extends Plugin {

  /**
   * The profiler plugin's internal ID, in machine name format.
   *
   * @var string
   */
  public $id;

  /**
   * The display label/name of the profiler plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A longer explanation of what the field is for.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The name of the field formatter class.
   *
   * This is not provided manually, it will be added by the discovery mechanism.
   *
   * @var string
   */
  public $class;

}
