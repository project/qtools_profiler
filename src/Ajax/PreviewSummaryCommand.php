<?php

namespace Drupal\qtools_profiler\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * An AJAX command for populating preview summary.
 */
class PreviewSummaryCommand implements CommandInterface {

  /**
   * Tracking id.
   *
   * @var int
   */
  protected $trackingId;

  /**
   * Request id.
   *
   * @var int
   */
  protected $requestId;

  /**
   * Name.
   *
   * @var array
   */
  protected $name;

  /**
   * Summary.
   *
   * @var array
   */
  protected $summary;

  /**
   * Constructs a PreviewSummaryCommand.
   */
  public function __construct($tracking_id, $request_id, $name, $summary) {
    $this->trackingId = $tracking_id;
    $this->requestId = $request_id;
    $this->name = $name;
    $this->summary = $summary;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    return [
      'command' => 'qtoolsProfilerPreviewSummary',
      'name' => $this->name,
      'summary' => json_encode($this->summary),
      'trackingId' => $this->trackingId,
      'requestId' => $this->requestId,
    ];
  }

}
