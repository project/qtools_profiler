<?php

namespace Drupal\qtools_profiler;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides Profiler plugin manager.
 */
class ProfilerPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Profiler',
      $namespaces,
      $module_handler,
      'Drupal\qtools_profiler\ProfilerPluginInterface',
      'Drupal\Component\Annotation\Plugin',
      ['Drupal\qtools_profiler\Annotation\Profiler']
    );
    $this->alterInfo('qtools_profiler_info');
    $this->setCacheBackend($cache_backend, 'qtools_profiler_info');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

}
