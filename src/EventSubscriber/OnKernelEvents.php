<?php

namespace Drupal\qtools_profiler\EventSubscriber;

use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Url;
use Drupal\dynamic_page_cache\EventSubscriber\DynamicPageCacheSubscriber;
use Drupal\qtools_profiler\PerformanceService;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Force anonymous user redirect to login page.
 */
class OnKernelEvents implements EventSubscriberInterface {

  /**
   * Drupal\qtools_profiler\PerformanceService.
   *
   * @var \Drupal\qtools_profiler\PerformanceService
   */
  protected $performanceService;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    PerformanceService $performanceService
  ) {
    $this->performanceService = $performanceService;
  }

  /**
   * Handles kernel terminate event.
   */
  public function onKernelTerminate(PostResponseEvent $event) {
    if ($this->performanceService->getRequestId() !== NULL) {
      $this->performanceService->monitoringFinish($event);
    }
  }

  /**
   * Handles kernel response event.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    $request_id = $this->performanceService->getRequestId();

    if (!empty($request_id)) {
      $this->performanceService->monitoringComplete($event);

      // Inject tracking id into response regardless of caching policy.
      $response = $event->getResponse();
      if ($response instanceof HtmlResponse) {
        // Get current attachment array.
        $attachments = $response->getAttachments();
        $attachments = $this->attachProfilerInfo($attachments, $request_id);


        // Get DC result.
        $dynamic_cache_status = $response->headers->get(DynamicPageCacheSubscriber::HEADER, 'UND');
        $page_cache_status = $response->headers->get('X-Drupal-Cache','UND');
        $attachments['html_head'][] = [
          [
            '#type' => 'html_tag',
            '#tag' => 'meta',
            '#attributes' => [
              'name' => 'qtools-profiler-cache-status',
              'id' => 'qtools-profiler-cache-status',
              'content' => $page_cache_status . ":" . $dynamic_cache_status,
            ],
            '#weight' => 1,
          ],
          'qtools_profiler_page_cache',
        ];

        $response->setAttachments($attachments);
      }
    }
  }

  /**
   * Inject profiler resources into page attachments.
   *
   * @return array
   *   Modified attachments array.
   */
  protected function attachProfilerInfo($attachments, $request_id) {
    // Add library to view page stats realtime.
    if (\Drupal::currentUser()->hasPermission('qtools_profiler.preview')) {
      if ($this->performanceService->confGet('preview_mode') == PerformanceService::PREVIEW_MODE_IFRAME) {
        $libriary = 'qtools_profiler/profiler-preview-iframe';
      }
      else {
        $libriary = 'qtools_profiler/profiler-preview-silent';
      }

      $attachments['library'][] = $libriary;
      $attachments['drupalSettings']['qtools_profiler']['request_id'] = $request_id;

      // List preview reports.
      $preview_reports = [];
      if ($this->performanceService->profilingStarted()) {
        $preview_reports[] = ['/admin/config/development/qtools_profiler/preview_profile/{id}', t('Profile')];
      }

      $moduleHandler = \Drupal::moduleHandler();

      // Default scales for cache contexts.
      $scales = [
        ['languages:language_interface$', count(\Drupal::languageManager()->getLanguages())],
        ['theme$', 1],
        ['timezone$', 1],
        ['user.roles:', 2],
        ['user.permissions$', 10],
        ['user$', 1000],
        ['url$', 1000],
        ['url.site$', 1],
        ['url.path.is_front$', 2],
        ['route$', 100],
        ['.*', 10],
      ];
      $moduleHandler->alter('qtools_profiler_contexts_scales', $scales);
      $attachments['drupalSettings']['qtools_profiler']['contexts_scales'] = $scales;

      // Allow other modules to add their own reports.
      $moduleHandler->alter('qtools_profiler_preview_reports', $preview_reports, $attachments);
      $base_url = rtrim(Url::fromUri('base:')->setAbsolute(TRUE)->toString(), '/');
      foreach ($preview_reports as &$preview_report) {
        $preview_report[0] = ($preview_report[0][0] != '#') ? $base_url . $preview_report[0] : $preview_report[0];
      }
      $attachments['drupalSettings']['qtools_profiler']['reports'] = $preview_reports;
    }

    return $attachments;
  }

  /**
   * Handles kernel request event.
   */
  public function onKernelRequestEarly(GetResponseEvent $event) {
    // Ensure cookie client.
    $this->performanceService->monitoringEnsureCookie($event);

    // Start monitoring if allowed.
    if ($this->performanceService->monitoringAllowed($event)) {
      $this->performanceService->monitoringStart($event);
    }
  }

  /**
   * Handles kernel request event.
   */
  public function onKernelRequestWithRoute(GetResponseEvent $event) {
    if ($this->performanceService->monitoringActive()) {
      $route = $event->getRequest()->attributes->get('_route');
      if (!empty($route)) {
        $this->performanceService->setRoute($route);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[KernelEvents::TERMINATE][] = 'onKernelTerminate';
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 10000];
    $events[KernelEvents::REQUEST][] = ['onKernelRequestEarly', 10000];
    $events[KernelEvents::REQUEST][] = ['onKernelRequestWithRoute', 31];
    return $events;
  }

}
