<?php

namespace Drupal\qtools_profiler;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface ProfilerPluginInterface.
 *
 * @package Drupal\qtools_profiler
 */
interface ProfilerPluginInterface extends PluginInspectionInterface {

  /**
   * Returns TRUE if this extension is loaded into the PHP interpreter.
   *
   * @return bool
   *   Flag if extension loaded.
   */
  public static function isLoaded();

  /**
   * Enable the extension.
   */
  public function enable($options);

  /**
   * Disable the extension.
   */
  public function disable();

  /**
   * Output profile report.
   */
  public function buildProfilePage(Request $request, $id);

  /**
   * Retrieve settings form for profiler.
   */
  public function getSettingsForm(array $conf);

}
