<?php

namespace Drupal\qtools_profiler\Plugin\Profiler;

use Blackfire\Client as BlackfireClient;
use Blackfire\ClientConfiguration;
use Blackfire\Exception\ApiException;
use Blackfire\Exception\ConfigNotFoundException;
use Blackfire\Exception\EnvNotFoundException;
use Blackfire\Exception\NotAvailableException;
use Blackfire\Exception\OfflineException;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\qtools_profiler\ProfilerPluginInterface;
use Drupal\salesforce\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides Blackfire profiler plugin.
 *
 * @Plugin(
 *   id = "blackfire",
 *   label = @Translation("Blackfire.io"),
 *   description = @Translation("Blackfire php profiler.")
 * )
 */
class Blackfire extends PluginBase implements ProfilerPluginInterface {

  /**
   * Blackfire Profile client.
   *
   * @var \Blackfire\Client
   */
  protected $blackfire;

  /**
   * Blackfire probe.
   *
   * @var \Blackfire\Probe
   */
  protected $probe;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function isLoaded() {
    return extension_loaded('blackfire');
  }

  /**
   * {@inheritdoc}
   */
  public function enable($options) {
    if (!static::isLoaded()) {
      return;
    }

    try {
      $config = new ClientConfiguration($options['client_id'], $options['client_token']);
      $this->blackfire = new BlackfireClient($config);
      $this->probe = $this->blackfire->createProbe();
    }
    catch (NotAvailableException $e) {
      // The Blackfire PHP extension is not installed or not enabled.
    }
    catch (OfflineException $e) {
      // You are offline or the Blackfire servers are not reachable (check your
      // proxy configuration).
    }
    catch (ApiException $e) {
      // An error occurred when communicating with Blackfire.
      if ($e->getCode() === 429) {
        // Too many requests while calling POST
        // https://blackfire.io/api/v1/signing.
      }
    }
    catch (ConfigNotFoundException $e) {
      // The Blackfire configuration file cannot be found.
    }
    catch (EnvNotFoundException $e) {
      // The configured environment does not exist.
    }

    catch (Exception $e) {
      // Some error.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function disable() {
    if ($this->probe) {
      $profile = $this->blackfire->endProbe($this->probe);
      // Get the main costs.
      $cost = $profile->getMainCost();
      $data = [
        'graph' => Link::fromTextAndUrl(t('View at Blackfire site'), Url::fromUri($profile->getUrl(), ['external' => TRUE]))->toString(),
        // Cost methods.
        // wt.
        'wall_time' => $cost->getWallTime() / 1000000 . ' sec',
        // cpu.
        'cpu_time' => $cost->getCpu(),
        // io.
        'io' => $cost->getIo(),
        // nw.
        'network' => $cost->getNetwork(),
        // pmu.
        'peak_memory' => $cost->getPeakMemoryUsage() / 1024 / 1024 . 'MB',
        'memory' => $cost->getMemoryUsage() / 1024 / 1024 . 'MB',
        'isSuccessful' => $profile->isSuccessful(),
        'isErrored' => $profile->isErrored(),
        'tests' => $profile->getTests(),
        'queries' => $profile->getSqls(),
      ];

      return $data;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildProfilePage(Request $request, $id) {
    // Build run from data.
    $reportService = \Drupal::service('qtools_profiler.report');
    $data = $reportService->getProfileDetails($id);
    if (empty($data)) {
      throw new NotFoundHttpException();
    }

    $rows = [];
    foreach ($data as $key => $value) {
      $rows[] = [
        'data' => [$key, $value],
      ];
    }

    $build['header'] = [
      '#markup' => 'Summary',
    ];
    $build['table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingsForm(array $conf) {
    // Id 4fd1e668-0af0-4a67-9729-9865573a4bfc.
    // Token 71990ab3f3d146736acc3e8e68fc082098e1d07234714dd25caece9c29330d3e.
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => t('Client id'),
      '#default_value' => $conf['profiling']['profilers']['blackfire']['client_id'] ?? '',
      '#description' => t('Blackfire cliet id'),
    ];
    $form['client_token'] = [
      '#type' => 'textfield',
      '#title' => t('Client token'),
      '#default_value' => $conf['profiling']['profilers']['blackfire']['client_token'] ?? '',
      '#description' => t('Blackfire client token'),
    ];
    return $form;
  }

}
