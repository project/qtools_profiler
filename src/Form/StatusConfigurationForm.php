<?php

namespace Drupal\qtools_profiler\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\qtools_profiler\PerformanceService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class StatusConfigurationForm.
 *
 * @package Drupal\qtools_profiler\Form
 */
class StatusConfigurationForm extends FormBase {

  /**
   * Drupal\qtools_profiler\PerformanceService.
   *
   * @var \Drupal\qtools_profiler\PerformanceService
   */
  protected $performanceService;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(PerformanceService $performanceService, MessengerInterface $messenger) {
    $this->performanceService = $performanceService;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('qtools_profiler.performance'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $conf = $this->performanceService->confGet();

    $form['conf'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Configuration'),
      '#tree' => TRUE,
    ];

    $form['conf']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => !empty($conf['enabled']),
    ];
    $form['conf']['preview_theme'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preview theme'),
      '#default_value' => $conf['preview_theme'],
    ];
    $form['conf']['preview_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Preview mode'),
      '#default_value' => $conf['preview_mode'],
      '#options' => [
        PerformanceService::PREVIEW_MODE_SILENT => $this->t('Silent'),
        PerformanceService::PREVIEW_MODE_IFRAME => $this->t('Iframe'),
      ],
    ];
    $form['conf']['cleanup'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cleanup ttl'),
      '#description' => $this->t('For how long keep records in db.'),
      '#default_value' => $conf['cleanup'],
    ];
    $form['conf']['flush'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flush logs at day start'),
      '#description' => $this->t('To prevent possible database creep its good to truncate tables from time to time, if enabled, flush operation will be done during first cron each day.'),
      '#default_value' => $conf['flush'],
    ];

    // Monitoring settings.
    $form['conf']['monitoring'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Monitoring'),
      '#tree' => TRUE,
    ];
    $form['conf']['monitoring']['rules'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Rules to match requests to monitor'),
      '#default_value' => $conf['monitoring']['rules'],
      '#description' => $this->t('format is {fraction};[path|role|uid]={value}'),
    ];
    $form['conf']['monitoring']['exclude'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Rules to exclude requests from monitor'),
      '#default_value' => $conf['monitoring']['exclude'],
      '#description' => $this->t('format is 1;[path|role|uid]={value}'),
    ];
    $form['conf']['monitoring']['dblogging'] = [
      '#type' => 'select',
      '#title' => $this->t('Monitor database queries'),
      '#options' => [
        PerformanceService::DB_MODE_NONE => $this->t('None'),
        PerformanceService::DB_MODE_TOTALS => $this->t('Totals'),
      ],
      '#description' => $this->t('Specify to which extent we should monitor database queries.'),
      '#default_value' => $conf['monitoring']['dblogging'] ?? PerformanceService::DB_MODE_NONE,
    ];

    // Profiler settings.
    $form['conf']['profiling'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Profiling'),
      '#tree' => TRUE,
    ];
    $profiler_extensions = $this->performanceService->getProfilerOptions();
    $form['conf']['profiling']['extension'] = [
      '#type' => 'select',
      '#title' => $this->t('Select profiler'),
      '#options' => $profiler_extensions,
      '#default_value' => $conf['profiling']['extension'],
    ];

    $form['conf']['profiling']['profilers'] = $this->performanceService->getProfilerOptionsForm();
    foreach ($form['conf']['profiling']['profilers'] as $key => $value) {
      $form['conf']['profiling']['profilers'][$key]['#states'] = [
        'visible' => [
          ':input[name="conf[profiling][extension]"]' => ['value' => $key],
        ],
      ];
    }

    $form['conf']['profiling']['rules'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Rules to match requests to profile'),
      '#default_value' => $conf['profiling']['rules'],
      '#description' => $this->t('format is {fraction};[path|role|uid]={value}'),
    ];
    $form['conf']['profiling']['cookie'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Profiler cookie value'),
      '#description' => $this->t('Cookie name is "qtools_profiler", enter "+" to generate random one. If less than 10 symbols will be ignored.'),
      '#default_value' => $conf['profiling']['cookie'],
    ];

    $form['flush'] = [
      '#type' => 'details',
      '#title' => $this->t('Flush all data'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    ];

    $form['flush']['flush_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flush all stats'),
      '#description' => $this->t('This action cannot be undone.'),
      '#default_value' => FALSE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $conf = $form_state->getValue('conf');

    // Check if we need to generate random profiler cookie.
    if ($conf['profiling']['cookie'] == '+') {
      $conf['profiling']['cookie'] = uniqid('pc', TRUE);
    }
    $this->performanceService->confSet($conf);
    $this->messenger->addMessage($this->t('Changes saved.'));

    // Check for log clear.
    $flush = $form_state->getValue('flush_all');
    if (!empty($flush)) {
      $this->performanceService->flush();
      $this->messenger->addMessage($this->t('All data deleted.'));
    }
  }

}
