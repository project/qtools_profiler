<?php

namespace Drupal\qtools_profiler\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\qtools_profiler\PerformanceService;

/**
 * Class QtoolsProfilerThemeswitcherNegotiator.
 */
class QtoolsProfilerThemeswitcherNegotiator implements ThemeNegotiatorInterface {

  /**
   * Drupal\qtools_profiler\PerformanceService.
   *
   * @var \Drupal\qtools_profiler\PerformanceService
   */
  protected $performanceService;

  /**
   * Constructor.
   */
  public function __construct(PerformanceService $performanceService) {
    $this->performanceService = $performanceService;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route_name = $route_match->getRouteName();
    $preview_theme = $this->performanceService->confGet('preview_theme');
    return !empty($route_name) && !empty($preview_theme) && strpos($route_name, 'qtools_profiler.preview.') !== FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->performanceService->confGet('preview_theme');
  }

}
