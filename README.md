README.md for QTools Profiler
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Description
 * Dependencies
 * Installation
 * Configuration

# Description

This module provides tools package for monitoring and profiling of any data during the request execution.

Out of the box Qtools Profiler module gives the ability to provide Database Queries monitoring and enable Page Profiling.

It supports 2 profilers :
- Blackfire.io
- Tideways xhprof 

The Profiler data is represented in 2 preview modes :

## Iframe
The Profiler tab appears on the bottom of the page. Content per each report is displayed in appropriate iframe.

## Silent 
Works in Google Chrome. Profiler tab appears in the Developer tools panel (chrome extension is bundled with this module).

# Dependencies

- QTools Common (https://www.drupal.org/project/qtools_common/) 

# Installation

- Download the module via Composer( composer require drupal/qtools_profiler );
- Install module and visit page /admin/config/development/qtools_profiler/config - implement configurations and Enable profiler

To use Silent preview mode you need to add Google Chrome extension QTools devtools.
To do this : 

- go to URL chrome://extensions
- enable Developer mode and click Load unpacked button.
- The unpacked extension is available in qtools_profiler module in directory react-app/chrome-extension - choose it and click Open.

If you are going to use PHP Calls/Time/Memory Profiling you need to install one of following extensions on your web server :

- Blackfire.io ( https://blackfire.io/docs/up-and-running/installation )
- Tideways xhprof ( https://tideways.com/profiler/xhprof-for-php7 )

If the following extension is not available - there will be a notice next to appropriate select option ( extension not loaded ).

# Configuration

On Monitoring configuration page you are able to configure profiling rules and basic settings /admin/config/development/qtools_profiler/config

## Preview theme

Use some specific theme for displaying reports preview.
It is convenient to avoid rendering extra page elements which are not needed on the report page.

### Path rules
You are able to enable (disable) profiler for appropriate pages, use specific format to configure path rules :
- {fraction};[path|route|role|uid]={value}

#### For example :

Enable profiling for all pages :
- 1|path=*

Here fraction = 1. It means - monitor every request.
If we set fraction = 10 than every 10th request will be monitored (can be used for performance needs, when you need to get some average statistics).

Exclude all admin routes from profiling :
- 1|path=/admin/*

Here - all requests on /admin pages will be ignored by profiler.

### Profiler cookie value

You can force enable profiling by cookie value.
If appropriate cookie is set - profiling will be displayed on the page, ignoring if Path Rules match or not.
This is needed if you want to enable profiling on a prod server and want only your session to be profiled (to avoid performance impact on live server)

