<?php

namespace Drupal\qtools_cache_profiler;

use Drupal\Core\Database\Connection;
use Drupal\Core\State\State;

/**
 * Class PerformanceService.
 *
 * @package Drupal\qtools_cache_profiler
 */
class PerformanceService {

  /**
   * Drupal\Core\Database\Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Drupal\Core\State\State.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Configuration.
   *
   * @var array
   */
  protected $configuration = [
    'logging_mode' => 0,
  ];

  // Cache actions.
  const CACHE_MISS = 0;
  const CACHE_SET = 1;
  const CACHE_HIT = 2;

  // Config key.
  const STATE_CONFIGURATION_KEY = 'qtools_cache_profiler_configuration';
  const CACHE_RENDER_STATS_KEY = 'qtools_cache_profiler_render_stats';

  /**
   * Constructor.
   */
  public function __construct(
    Connection $connection,
    State $state
  ) {
    $this->connection = $connection;
    $this->state = $state;

    // Read configuration from state and merge with defaults.
    $activeConfiguration = $this->state->get(static::STATE_CONFIGURATION_KEY, []);
    foreach ($this->configuration as $key => $value) {
      if (isset($activeConfiguration[$key])) {
        $this->configuration[$key] = $activeConfiguration[$key];
      }
    }
  }

  /**
   * Saves render cache stats.
   */
  public function saveRenderCacheStats($rid, $render_cache_stats) {
    $cached = \Drupal::cache()->get(static::CACHE_RENDER_STATS_KEY);
    if ($cached) {
      $stats = $cached->data;
    }
    else {
      $stats = [];
    }
    // For now just save last 100 records.
    $stats[$rid] = $render_cache_stats;
    foreach (array_keys($stats) as $srid) {
      if ($srid < $rid - 100) {
        unset($stats[$srid]);
      }
    }

    \Drupal::cache()->set(static::CACHE_RENDER_STATS_KEY, $stats, time() + 60);
  }

  /**
   * Gets render cache stats.
   */
  public function getRenderCacheStats($rid) {
    $cached = \Drupal::cache()->get(static::CACHE_RENDER_STATS_KEY);
    if ($cached) {
      $stats = $cached->data;
    }
    else {
      $stats = [];
    }
    return !empty($stats[$rid]) ? $stats[$rid] : [];
  }

  /**
   * Determine if performance service is currently active.
   */
  public function enabled() {
    // @todo: real check.
    return TRUE;
  }

  /**
   * Returns current login mode.
   */
  public function getLoggingMode() {
    return $this->confGet('logging_mode');
  }

  /**
   * Returns current configuration.
   *
   * @return array|mixed
   *   Configuration.
   */
  public function confGet($key = NULL) {
    return empty($key) ? $this->configuration : $this->configuration[$key];
  }

  /**
   * Returns current configuration.
   */
  public function confSet($newConfiguration) {
    foreach ($this->configuration as $key => $value) {
      if (isset($newConfiguration[$key])) {
        $this->configuration[$key] = $newConfiguration[$key];
      }
    }

    // Cleanup logs on each logs disable.
    if ($this->configuration['logging_mode'] == static::LOG_DETAILS_NONE) {
      $this->logsCleanUp();
    }
    $this->state->set(static::STATE_CONFIGURATION_KEY, $this->configuration);
  }

  /**
   * Truncates error logs.
   */
  protected function logsCleanUp() {
  }

  /**
   * Flush all logs.
   */
  public function flush() {
  }

  /**
   * Flush all logs up until given request id.
   */
  public function cleanup($rid) {
  }

  /**
   * Get Request details.
   *
   * @return array|null
   *   Summary definitions array.
   */
  public function getRequestSummary($requestId) {
    return [];
  }

}
