<?php

namespace Drupal\qtools_cache_profiler;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Modifies ServiceProviderBase service.
 *
 * @package Drupal\qtools_cache_profiler
 */
class QtoolsCacheProfilerServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {

    // Overrides render_cache service with one that logs cache calls.
    $definition = $container->getDefinition('render_cache');
    $definition->setClass('Drupal\qtools_cache_profiler\PlaceholderingRenderCacheWrapper')
      ->addArgument(new Reference('qtools_cache_profiler.performance'));

    // Overrides renderer service so we can output render info on the page.
    $container->getDefinition('renderer')->setClass('Drupal\renderviz\Renderer');
    $definition = $container->getDefinition('renderer');
    $definition->setClass('Drupal\qtools_cache_profiler\Renderer');
  }

}
