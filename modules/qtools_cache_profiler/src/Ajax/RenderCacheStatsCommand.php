<?php

namespace Drupal\qtools_cache_profiler\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * An AJAX command for populating render cache metadata.
 */
class RenderCacheStatsCommand implements CommandInterface {

  /**
   * Stats.
   *
   * @var array
   */
  protected $stats;

  /**
   * Constructs a RenderCacheStatsCommand.
   */
  public function __construct($stats) {
    $this->stats = $stats;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    return [
      'command' => 'qtoolsCacheProfilerRenderCacheStats',
      'stats' => $this->stats,
    ];
  }

}
