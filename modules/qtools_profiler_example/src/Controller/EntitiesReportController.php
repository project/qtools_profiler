<?php

namespace Drupal\qtools_profiler_example\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\qtools_profiler_example\PerformanceService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EntitiesReportController.
 *
 * @package Drupal\qtools_profiler_example\Controller
 */
class EntitiesReportController extends ControllerBase {

  /**
   * Drupal\qtools_profiler_example\PerformanceService.
   *
   * @var \Drupal\qtools_profiler_example\PerformanceService
   */
  protected $performanceService;

  /**
   * {@inheritdoc}
   */
  public function __construct(PerformanceService $performanceService) {
    $this->performanceService = $performanceService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('qtools_profiler_example.performance')
    );
  }

  /**
   * Preview report.
   */
  public function previewReport(Request $request) {
    $request_id = $request->query->get('request');

    return [
      '#markup' => $this->t('Entity loads count @count', ['@count' => $this->performanceService->getPerformanceStat($request_id)]),
    ];
  }

}
