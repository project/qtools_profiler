// Make sure that extension can embed iframes.
// @see https://stackoverflow.com/a/15534822/8625546
var trustedHosts = [];
chrome.webRequest.onHeadersReceived.addListener(
    function(info) {
      // Skip iframes that are outside of our extension.
      var url = new URL(info.url);

      // Double check for self origin.
      if (trustedHosts.indexOf(url.hostname) === -1) {
        if (window.origin !== info.initiator) {
          return;
        }
        // Add to trusted targets.
        trustedHosts.push(url.hostname);
      }

      // Remove iframe restriction origin.
      var headers = info.responseHeaders;
      for (var i=headers.length-1; i>=0; --i) {
        var header = headers[i].name.toLowerCase();
        if (header === 'x-frame-options' || header === 'frame-options') {
          headers.splice(i, 1); // Remove header
        }
      }
      return {responseHeaders: headers};
    },
    {
      urls: [ '*://*/*' ], // Pattern to match all http(s) pages
      types: [ 'sub_frame' ]
    },
    ['blocking', 'responseHeaders']
);

// Reconnect extension on page reload.
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab){
  if (changeInfo.status === 'complete') {
    var message = {
      data: {
        tabId: tabId
      },
      type: 'BG_TAB_LOADED',
      source: 'background',
      version: 1
    };
    chrome.runtime.sendMessage({data: message});
  }
});
