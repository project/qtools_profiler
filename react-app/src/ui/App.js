import React, { Fragment } from 'react';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/styles';
import { connect } from 'react-redux';

import ReportsSelectionHeader from './components/ReportsSelectionHeader'
import RequestsList from './components/RequestsList'
import SideMenu from './components/SideMenu'
import ReportDetails from './components/ReportDetails'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  side: {
    width: 200,
    height: '100vh',
    borderRight: '1px grey solid',
    backgroundColor: '#eee'
  },
  hidden: {
    display: 'none',
  }
});

// const reports = [
//     {
//       id: 'xhprof',
//       label: 'Profile',
//       url: '/profile',
//       requestId: true
//     },
//     {
//       id: 'services',
//       label: 'Services',
//       url: '/services',
//       requestId: true
//     },
//     {
//       id: 'server',
//       label: 'Server state',
//       url: '/server',
//       requestId: false
//     }
// ];

const App = ({ reports, classes, showSummary, showRequests, requestsCount }) => {

  return (
    <div className={classes.root}>
      <ReportsSelectionHeader reports={reports} requestsCount={requestsCount} />

      <Grid container>
        <Grid item className={showSummary ? classes.side : classes.hidden}>
          <SideMenu reports={reports} requestsCount={requestsCount}/>
        </Grid>

        <Grid item {...(showSummary ? {xs: 12, sm: true} : {})} className={showSummary ? '' : (showRequests ? classes.side : classes.hidden)}>
          <RequestsList showSummary={showSummary}/>
        </Grid>

        <Grid item xs={12} sm className={showSummary ? classes.hidden : classes.side}>
          <ReportDetails />
        </Grid>

      </Grid>
    </div>
  )
}

export default connect(
  state => ({
    showSummary: (state.currentReport === '_'),
    showRequests: (state.reports[state.currentReport] && state.reports[state.currentReport].type === 'REQUEST'),
    requestsCount: state.requests.length,
    reports: state.reports
  }),
  dispatch => ({})
)(withStyles(styles)(App));
