import { createMuiTheme } from '@material-ui/core/styles';

const palette = {
  // primary: { main: '#111111' },
  // secondary: { main: '#757575' }
  // type: 'dark'
};
const themeName = 'Dark theme';

export default createMuiTheme({ palette, themeName });
