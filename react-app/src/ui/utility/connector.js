const inspectorInfo = {
  id: Date.now(),
  mode: ((typeof chrome !== 'undefined') && chrome.devtools && chrome.devtools.inspectedWindow) ? 'devtools' : 'iframe',
  tabId: ((typeof chrome !== 'undefined') && chrome.devtools && chrome.devtools.inspectedWindow) ? chrome.devtools.inspectedWindow.tabId : null
}

export function getInspectorInfo() {
  return inspectorInfo;
}

export function connectInspector(store) {
  const inspectorInfo = getInspectorInfo();

  // Start listening for incoming messages.
  if (inspectorInfo.mode === 'devtools') {
    chrome.runtime.onMessage.addListener(function (e) {
      processMessage(e, store);
    });
  }
  else {
    window.addEventListener('message', (e) => {
      processMessage(e, store);
  });
  }

  // Send connect message.
  sendMessage('CONNECT', getInspectorInfo())
}

function processMessagePreview(type, data, store) {
  if (type === 'REQUESTS_SET') {
    store.dispatch({type: 'REQUESTS_SET', data: data.data});
  }
  else if (type === 'REPORTS_SET') {
    store.dispatch({type: 'REPORTS_SET', data: data.data});
  }
  else if (type === 'REPORT_CONTENT_SET') {
    store.dispatch({type: 'REPORT_CONTENT_SET', data: data.data});
  }
  else if (type === 'FORCE_REPORT') {
    store.dispatch({type: 'SELECT_REPORT', data: data.data.report});
  }
  else {
    console.log('Unhandled message', e);
  }
}

export function processMessage(e, store) {
  console.log('[Inspector] processMessage', e);
  if (e.data && e.data.type) {
    const inspectorInfo = getInspectorInfo();
    if (e.data.source === 'preview') {
      if (e.data.inspector && e.data.inspector.id === inspectorInfo.id) {
        processMessagePreview(e.data.type, e.data, store);
      }
      else {
        console.log('Another inspector message', e);
      }
    }
    else if (e.data.source === 'background') {
      if (e.data.data && e.data.data.tabId === inspectorInfo.tabId) {
        // Send reconnect message.
        sendMessage('CONNECT', getInspectorInfo())
      }
      else {
        console.log('Another tab message', e);
      }
    }
    else {
      console.log('Invalid message', e);
    }
  }
  else {
    // console.log('Invalid message', e);
  }

}

export function sendMessage(type, data = {}) {
  // Build message object.
  const message = {
    type,
    data,
    version: 1
  }

  // Check id we are running from extension.
  if (inspectorInfo.mode === "devtools") {
    message.mode = 'devtools';
    chrome.devtools.inspectedWindow.eval("window.postMessage(" + JSON.stringify(message)  + ", '*');");
  }
  else {
    if (window.parent) {
      message.mode = 'iframe';
      window.parent.postMessage(message, '*');
    }
  }

  console.log('[Inspector] sendMessage', message);
}
