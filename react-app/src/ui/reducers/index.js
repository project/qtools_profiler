import { combineReducers } from 'redux';
import requests from './requests';
import reports from './reports';
import reportsContent from './reportsContent';
import currentRequest from './currentRequest';
import currentReport from './currentReport';
import currentFold from './currentFold';

export default combineReducers({
  requests,
  reports,
  reportsContent,
  currentRequest,
  currentReport,
  currentFold
})
