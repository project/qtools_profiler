import { sendMessage } from '../utility/connector'
const initialState = '_';

function currentReport(state = initialState, action) {
  if (action.type === 'SELECT_REPORT') {
    let newReport = (action.data !== null && action.data !== undefined) ? action.data : '_';
    sendMessage('SELECT_REPORT', {report: newReport});
    return newReport;
  }
  return state;
}

export default currentReport;
