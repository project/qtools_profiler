const initialState = [];

function reports(state = initialState, action) {
  if (action.type === 'REPORTS_SET') {
    let reports = [];
    action.data.map((info, index) => {
      let type = 'STATE';
      if (info[0].indexOf('{id}') !== -1) {
        type = 'REQUEST';
      }
      else if (info[0].indexOf('#') === 0) {
        type = 'INSPECT';
      }
      reports.push({
        id: index,
        url: info[0],
        name: info[1],
        type: type
      });
    });
    return reports;
  }
  return state;
}

export default reports;
