import { sendMessage } from '../utility/connector'
const initialState = '';

function currentFold(state = initialState, action) {
  if (action.type === 'SET_FOLD') {
    let newState = action.data;
    if (newState === '-') {
      newState = '';
      sendMessage('MINIMIZE');
    }
    else if (newState === '+') {
      sendMessage('MAXIMIZE');
    }
    else {
      sendMessage('NORMALIZE');
    }

    return newState;
  }
  return state;
}

export default currentFold;
