const initialState = {};

function reportsContent(state = initialState, action) {
  if (action.type === 'REPORT_CONTENT_SET') {
    const report = action.data.report;
    let newState = {
      ...state,
      [report]: action.data.content
    }
    return newState;
  }
  return state;
}

export default reportsContent;
