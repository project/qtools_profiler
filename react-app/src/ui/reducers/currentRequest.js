const initialState = 0;

function currentRequest(state = initialState, action) {
  if (action.type === 'SELECT_REQUEST') {
    return action.data;
  }
  return state;
}

export default currentRequest;
