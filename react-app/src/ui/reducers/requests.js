const initialState = [
  {
    name: 'DOC',
    trackingId: 0,
    requestId: 0,
    summary: []
  },
];

function requests(state = initialState, action) {
  if (action.type === 'REQUESTS_SET') {
    return action.data;
  }
  return state;
}

export default requests;
