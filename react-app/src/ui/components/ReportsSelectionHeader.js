import React from 'react';
import { connect } from 'react-redux';

import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import NormalizeIcon from '@material-ui/icons/UnfoldLess';
import MaximizeIcon from '@material-ui/icons/UnfoldMore';
import MinimizeIcon from '@material-ui/icons/TransitEnterexit';

import Button from '@material-ui/core/Button';

import { sendMessage, getInspectorInfo } from '../utility/connector';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const MinimizeButton = React.forwardRef(
  (props, ref) => {
    return (
      <Button color="inherit" ref={ref} onClick={props.minimize}>
        <MinimizeIcon />
      </Button>
    )
  }
)
const ConnectedMinimizeButton = connect(
  state => ({}),
  dispatch => ({
    minimize: () => {
      console.log('minimize');
      dispatch({ type: 'SET_FOLD', data: '-'});
    }
  })
)(MinimizeButton);

const FoldButton = React.forwardRef(
  (props, ref) => {
    return (
      <Button color="inherit" ref={ref} onClick={props.currentFold === '+' ? props.normalize : props.maximize }>
        {
          props.currentFold === '+' &&
            <NormalizeIcon /> ||
            <MaximizeIcon />
        }
      </Button>
    )
  }
)

const ConnectedFoldButton = connect(
  state => ({}),
  dispatch => ({
    maximize: () => {
      dispatch({ type: 'SET_FOLD', data: '+'});
    },
    normalize: () => {
      dispatch({ type: 'SET_FOLD', data: ''});
    }
  })
)(FoldButton);

const SummaryButton = React.forwardRef(
  (props, ref) => {
    const goToSummary = () => {
      props.deselectReport();
    }

    return (
      <Button color="inherit" ref={ref} onClick={goToSummary}>
        <MenuIcon />
      </Button>
    )
  }
)

const ConnectedSummaryButton = connect(
  state => ({}),
  dispatch => ({
    deselectReport: () => {
      dispatch({ type: 'SELECT_REPORT', data: '_'});
    }
  })
)(SummaryButton);

const ReportsSelectionHeader = ({ reports, currentReport, currentFold, selectReport }) => {
  const classes = useStyles();

  function handleChange(event, newValue) {
    if (currentReport === newValue) {
      newValue = '_';
    }
    selectReport(newValue);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static">

        <Tabs value={(currentReport !== '_') ? currentReport : false} onChange={handleChange}>
          {
            getInspectorInfo().mode === 'iframe' &&
              <ConnectedMinimizeButton />
          }

          {
            getInspectorInfo().mode === 'iframe' &&
              <ConnectedFoldButton currentFold={currentFold} />
          }

          <ConnectedSummaryButton />
          {
            reports.length === 0 &&
              <Tab disabled />
          }

          {reports.map((item, index) =>
              <Tab value={item.id} key={item.id} label={item.name} />
          )}
        </Tabs>
      </AppBar>
    </div>
  );
}

export default connect(
  state => ({
    currentFold: state.currentFold,
    currentReport: state.currentReport,
    reportsCount: state.reports.length
  }),
  dispatch => ({
    selectReport: (reportId) => {
      dispatch({ type: 'SELECT_REPORT', data: reportId});
    }
  })
)(ReportsSelectionHeader);
