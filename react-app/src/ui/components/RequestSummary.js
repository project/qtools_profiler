import React, { Fragment } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tooltip from '@material-ui/core/Tooltip';

import DocumentIcon from '@material-ui/icons/Description';
import RedirectIcon from '@material-ui/icons/Label';
import AjaxIcon from '@material-ui/icons/LabelImportant';
import FailedIcon from '@material-ui/icons/LabelOff';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  requestName: {
    width: theme.spacing(10),
  }
}));

const RequestSummary = ({ request, showSummary }) => {

  const classes = useStyles();

  return (
    <Fragment>

      {
        showSummary &&
          <ListItemIcon>
            {
              request.trackingId === 0 &&
                <DocumentIcon />
              ||
                <AjaxIcon />
            }
          </ListItemIcon>
      }

      <Tooltip title={request.path || ''}>
        <ListItemText
          className={classes.requestName}
          primary={request.name}
        />
      </Tooltip>

      {
        showSummary && request.summary &&
          request.summary.map((item, index) =>
            <ListItemText key={index}
              primary={item[1]}
              secondary={item[0] + ((item[2] !== "") ? ', ' : '')  + item[2]}
            />
          )
      }
    </Fragment>
  )
}

export default RequestSummary;
