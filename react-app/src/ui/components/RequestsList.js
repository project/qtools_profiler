import React from 'react'
import { connect } from 'react-redux';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/styles';

import RequestSummary from './RequestSummary';

const styles = theme => ({
  noPadding: {
    padding: 0,
    height: '100vh',
    backgroundColor: theme.palette.background.paper
  }
});

const RequestsList = ({ classes, requests, currentRequest, showSummary, selectRequest }) => {

  function handleChange(trackingId) {
    selectRequest(trackingId);
  }

  return (
    <List dense={true} className={classes.noPadding}>
      {requests.map((item, index) =>
        <ListItem button key={index} selected={item.trackingId === currentRequest} onClick={handleChange.bind(null, item.trackingId)}>
          <RequestSummary request={item} showSummary={showSummary} />
        </ListItem>
      )}
    </List>
  )
}

export default connect(
  state => ({
    requests: state.requests,
    currentRequest: state.currentRequest
    // showSummary: (state.currentReport === '_')
  }),
  dispatch => ({
    selectRequest: (trackingId) => {
      dispatch({ type: 'SELECT_REQUEST', data: trackingId});
    }
  })
)(withStyles(styles)(RequestsList));
