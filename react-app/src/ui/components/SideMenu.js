import React from 'react';
import { connect } from 'react-redux';

import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import RequestsIcon from '@material-ui/icons/SwapHoriz';
import ServerInfoIcon from '@material-ui/icons/Receipt';

const useStyles = makeStyles(theme => ({
  noPadding: {
    padding: 0
  }
}));

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

function SideMenu({ reports, requestsCount, currentReport }) {
  const classes = useStyles();

  return (
    <div>
      <List component="nav" className={classes.noPadding}>
        <ListItem button>
          <ListItemIcon>
            <RequestsIcon />
          </ListItemIcon>
          <ListItemText primary={"Requests ( " + requestsCount + " )"} />
        </ListItem>
        <Divider />
        </List>
    </div>
  );
}

export default connect(
  state => ({
    currentReport: state.currentReport
  }),
  dispatch => ({
    selectReport: (reportId) => {
      dispatch({ type: 'SELECT_REPORT', data: reportId});
    }
  })
)(SideMenu);
