import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const ReportDetails = ({ report, reportsContent, request }) => {
  const styles = {width: '100%', height: '100vh', border: 0};
  return (
    <Fragment>
      {
        request && report && report.type != 'INSPECT' &&
          <iframe style={styles} src={report.url.replace('{id}', request.requestId)} />
      }
      {
        request && report && report.type == 'INSPECT' &&
          <pre style={styles}>
            {
              reportsContent[report.url] &&
              JSON.stringify(reportsContent[report.url], null,2)
            }
          </pre>
      }

    </Fragment>
  )
}

export default connect(
  state => ({
    report: state.reports[state.currentReport] || null,
    request: state.requests[state.currentRequest] || null,
    reportsContent: state.reportsContent
  }),
  dispatch => ({})
)(ReportDetails);
