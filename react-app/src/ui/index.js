import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { ThemeProvider } from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import App from './App'
import reducer from './reducers';
import { connectInspector, getInspectorInfo } from './utility/connector';
import grey from '@material-ui/core/colors/grey';

const store = createStore(reducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

const theme = createMuiTheme({
  palette: {
    // primary: grey,
    // type: 'dark'
  },
});

window.addEventListener('load', function() {
  ReactDOM.render(
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <App />
      </Provider>
    </MuiThemeProvider>
  , document.getElementById('container'));

  connectInspector(store);
})
