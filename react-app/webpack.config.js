const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {

  entry: {
    vendor: ['react', 'react-dom'],
    ui: './src/ui/index.js',
  },

  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          enforce: true
        },
      }
    },
    runtimeChunk: true
  },

  devtool: 'source-map',

  devServer: {
    contentBase: path.join(__dirname, 'chrome-extension/build/'),
    compress: true,
    disableHostCheck: true,
    host: '0.0.0.0',
    port: 3000
  },

  watchOptions: {
    ignored: /node_modules/,
    aggregateTimeout: 10,
    poll: 5
  },

  output: {
    path: path.join(__dirname, 'chrome-extension/build/'),
    publicPath: 'build',
    filename: '[name].bundle.js'
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: {
          loader: "html-loader"
        }
      }
    ]
  },

  plugins: [
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ]
};
